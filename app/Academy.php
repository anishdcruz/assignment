<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Academy extends Model
{
    protected $fillable = [
        'username',
        'email',
        'phone',
        'academy_name',
        'image',
        'description',
        'latitude',
        'longitude'
    ];

    protected $casts = [
        'latitude' => 'float',
        'longitude' => 'float'
    ];

    public function tags()
    {
        return $this->hasMany(AcademyTag::class);
    }

    public function timeSlots()
    {
        return $this->hasMany(AcademyTimeSlot::class);
    }
}
