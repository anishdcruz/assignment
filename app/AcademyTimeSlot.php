<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcademyTimeSlot extends Model
{
    protected $fillable = ['day_of_week', 'time_slot'];
}
