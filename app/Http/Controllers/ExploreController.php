<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Academy;
use Mail;

class ExploreController extends Controller
{
    public function index()
    {
        $academies = Academy::get(['id', 'academy_name', 'image', 'latitude', 'longitude'])
            ->toJson();
        return view('explore.index', [
            'title' => 'Explore Academies',
            'academies' => $academies
        ]);
    }

    public function show($id)
    {
        $academy = Academy::findOrFail($id);

        Mail::queue('emails.visit', ['academy' => $academy], function ($m) use ($academy) {
            $m->from('sandbox@sparkpostbox.com', 'Your Application');

            $m->to(env('ADMIN_EMAIL'), 'Assignment')->subject($academy->academy_name.' have a new Vistor!');
        });

        return view('explore.show', [
            'title' => 'Explore '.$academy->academy_name,
            'academy' => $academy
        ]);
    }
}
