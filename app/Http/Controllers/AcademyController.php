<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Academy;
use App\AcademyTimeSlot;
use App\AcademyTag;
use File;

class AcademyController extends Controller
{
    public function index()
    {
        $academies = Academy::orderBy('created_at', 'desc')
            ->get();

        return view('academy.index', [
            'title' => 'Manage Academy',
            'academies' => $academies
        ]);
    }

    public function create()
    {
        return view('academy.create', [
            'title' => 'Create Academy'
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|alpha_num|between:5,255|unique:academies',
            'email' => 'required|email|max:255',
            'phone' => 'required|max:255',
            'academy_name' => 'required|max:255',
            'image' => 'required|image|max:2000',
            'description' => 'required|max:2000',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'tags' => 'required|array',
            'time_slots.*' => 'max:255'
        ]);

        $imageName = null;

        if($request->hasFile('image')) {
            $imageName = str_random(16).'.'.$request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(public_path('uploads'), $imageName);
        }

        $academy = Academy::create([
            'username' => $request->username,
            'email' => $request->email,
            'phone' => $request->phone,
            'academy_name' => $request->academy_name,
            'image' => $imageName,
            'description' => $request->description,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude
        ]);

        $slots = [];

        foreach($request->get('time_slots') as $key => $timeSlot) {
            $slots[] = new AcademyTimeSlot([
                'day_of_week' => $key,
                'time_slot' => $timeSlot
            ]);
        }

        $tags = [];

        foreach($request->get('tags') as $tag) {
            $tags[] = new AcademyTag([
                'tag_name' => $tag
            ]);
        }

        $academy->tags()->saveMany($tags);
        $academy->timeSlots()->saveMany($slots);

        return redirect()
            ->route('academy.index')
            ->withMessage('Successfully create Academy!');
    }

    public function show(Academy $academy)
    {
        return view('academy.show', [
            'title' => 'View Academy',
            'academy' => $academy
        ]);
    }

    public function edit(Academy $academy)
    {
        return view('academy.edit', [
            'title' => 'Edit Academy',
            'academy' => $academy
        ]);
    }

    public function update(Academy $academy, Request $request)
    {
        $this->validate($request, [
            'username' => 'required|alpha_num|between:5,255|unique:academies,username,'.$academy->id,
            'email' => 'required|email|max:255',
            'phone' => 'required|max:255',
            'academy_name' => 'required|max:255',
            'image' => 'image|max:2000',
            'description' => 'required|max:2000',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'tags' => 'required|array',
            'time_slots.*' => 'max:255'
        ]);

        $imageName = $academy->image;

        if($request->hasFile('image')) {
            File::delete(public_path('uploads/'.$academy->image));
            $imageName = str_random(16).'.'.$request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(public_path('uploads'), $imageName);
        }

        $academy->update([
            'username' => $request->username,
            'email' => $request->email,
            'phone' => $request->phone,
            'academy_name' => $request->academy_name,
            'image' => $imageName,
            'description' => $request->description,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude
        ]);

        $academy->tags()->delete();
        $tags = [];

        foreach($request->get('tags') as $tag) {
            $tags[] = new AcademyTag([
                'tag_name' => $tag
            ]);
        }

        $academy->load('timeSlots');

        $slots = [];

        foreach($request->get('time_slots') as $key => $timeSlot) {

            if($current = $academy->timeSlots->where('day_of_week', $key)->first()) {
                $current->time_slot = $timeSlot;
                $current->save();
            }
        }

        $academy->tags()->saveMany($tags);

        return redirect()
            ->route('academy.index')
            ->withMessage('Successfully updated Academy!');
    }

    public function destroy(Academy $academy)
    {
        File::delete(public_path('uploads/'.$academy->image));
        $academy->tags()->delete();
        $academy->timeSlots()->delete();
        $academy->delete();

        return redirect()
            ->route('academy.index')
            ->withMessage('Successfully deleted Academy!');
    }
}
