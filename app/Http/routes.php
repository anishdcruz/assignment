<?php

Route::get('/', 'StaticController@index');
Route::resource('academy', 'AcademyController');
Route::resource('explore', 'ExploreController',['only' => [
    'index', 'show'
]]);