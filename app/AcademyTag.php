<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcademyTag extends Model
{
    protected $fillable = ['tag_name'];
}
