@extends('layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <p class="panel-title">Create Academy</p>
        </div>
        <div class="panel-body">
            <form action="{{route('academy.store')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <h1>Basic Information</h1>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" name="username" value="{{old('username')}}" class="form-control">
                            @if($errors->has('username'))
                                <p class="text-danger">{{$errors->first('username')}}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Email Address</label>
                            <input type="text" name="email" class="form-control">
                            @if($errors->has('email'))
                                <p class="text-danger">{{$errors->first('email')}}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Telephone</label>
                            <input type="text" name="phone" class="form-control">
                            @if($errors->has('phone'))
                                <p class="text-danger">{{$errors->first('phone')}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Academy Name</label>
                            <input type="text" name="academy_name" class="form-control">
                            @if($errors->has('academy_name'))
                                <p class="text-danger">{{$errors->first('academy_name')}}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Upload Image</label>
                            <input type="file" name="image" class="form-control">
                            @if($errors->has('image'))
                                <p class="text-danger">{{$errors->first('image')}}</p>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label>Latitude</label>
                                <input type="text" name="latitude" class="form-control">
                                @if($errors->has('latitude'))
                                    <p class="text-danger">{{$errors->first('latitude')}}</p>
                                @endif
                            </div>
                            <div class="col-sm-6 form-group">
                                <label>Longitude</label>
                                <input type="text" name="longitude" class="form-control">
                                @if($errors->has('longitude'))
                                    <p class="text-danger">{{$errors->first('longitude')}}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" class="form-control"></textarea>
                            @if($errors->has('description'))
                                <p class="text-danger">{{$errors->first('description')}}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Tags</label>

                            <div class="select-box clearfix form-control">
                                <span v-for="tag in selected" track-by="$index" class="select-box-item label label-info">
                                    <input type="hidden" name="tags[@{{$index}}]" :value="tag">
                                    @{{tag}}
                                    <span @click="remove(tag)">&times;</span>
                                </span>
                                <input type="text" class="select-input" v-model="tag_input" @keyup.delete="clear" @keyup.space="add">
                            </div>
                            @if($errors->has('tags'))
                                <p class="text-danger">{{$errors->first('tags')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <h2>Time Slots</h2>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Sunday</label>
                            <input type="text" name="time_slots[sunday]" class="form-control">
                            @if($errors->has('time_slots.sunday'))
                                <p class="text-danger">{{$errors->first('time_slots.sunday')}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Monday</label>
                            <input type="text" name="time_slots[monday]" class="form-control">
                            @if($errors->has('time_slots.monday'))
                                <p class="text-danger">{{$errors->first('time_slots.monday')}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Tuesday</label>
                            <input type="text" name="time_slots[tuesday]" class="form-control">
                            @if($errors->has('time_slots.tuesday'))
                                <p class="text-danger">{{$errors->first('time_slots.tuesday')}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Wednesday</label>
                            <input type="text" name="time_slots[wednesday]" class="form-control">
                            @if($errors->has('time_slots.wednesday'))
                                <p class="text-danger">{{$errors->first('time_slots.wednesday')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Thursday</label>
                            <input type="text" name="time_slots[thursday]" class="form-control">
                            @if($errors->has('time_slots.thursday'))
                                <p class="text-danger">{{$errors->first('time_slots.thursday')}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Friday</label>
                            <input type="text" name="time_slots[friday]" class="form-control">
                            @if($errors->has('time_slots.friday'))
                                <p class="text-danger">{{$errors->first('time_slots.friday')}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Saturday</label>
                            <input type="text" name="time_slots[saturday]" class="form-control">
                            @if($errors->has('time_slots.saturday'))
                                <p class="text-danger">{{$errors->first('time_slots.saturday')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" value="Create Academy" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        window.import = {
            tags: {!! json_encode(old('tags', [])) !!}
        };
    </script>
    <script type="text/javascript" src="{{asset('js/create.js')}}"></script>
@stop