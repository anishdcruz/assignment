@extends('layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <p class="panel-title">
                Manage Academies
                <a href="{{route('academy.create')}}" class="pull-right">Create Academy</a>
            </p>

        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Academy Name</th>
                                <th>Email Address</th>
                                <th>Telephone</th>
                                <th colspan="4">Created</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($academies as $academy)
                                <tr>
                                    <td>{{$academy->academy_name}}</td>
                                    <td>{{$academy->email}}</td>
                                    <td>{{$academy->phone}}</td>
                                    <td>{{$academy->created_at->diffForHumans()}}</td>
                                    <td><a href="{{route('academy.edit', $academy)}}">Edit</a></td>
                                    <td><a href="{{route('academy.show', $academy)}}">View</a></td>
                                    <td>
                                        <form action="{{route('academy.destroy', $academy)}}"
                                            onsubmit="return confirm('Are you sure?')"
                                            method="post" class="form-inline">
                                            <input type="hidden" name="_method" value="delete">
                                            {{csrf_field()}}
                                            <input type="submit" value="Delete" class="btn-delete">
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop