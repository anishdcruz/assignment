@extends('layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <p class="panel-title">
            Create Academy
            <a href="{{route('academy.show', $academy)}}" class="pull-right">View</a>
            </p>
        </div>
        <div class="panel-body">
            <form action="{{route('academy.update', $academy)}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="PUT">
                <h1>Basic Information</h1>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" name="username" value="{{old('username', $academy->username)}}" class="form-control">
                            @if($errors->has('username'))
                                <p class="text-danger">{{$errors->first('username')}}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Email Address</label>
                            <input type="text" name="email" value="{{old('email', $academy->email)}}" class="form-control">
                            @if($errors->has('email'))
                                <p class="text-danger">{{$errors->first('email')}}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Telephone</label>
                            <input type="text" name="phone" value="{{old('phone', $academy->phone)}}" class="form-control">
                            @if($errors->has('phone'))
                                <p class="text-danger">{{$errors->first('phone')}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Academy Name</label>
                            <input type="text" name="academy_name" value="{{old('academy_name', $academy->academy_name)}}" class="form-control">
                            @if($errors->has('academy_name'))
                                <p class="text-danger">{{$errors->first('academy_name')}}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Upload Image <small class="text-info">Optional</small></label>
                            <input type="file" name="image" class="form-control">
                            @if($errors->has('image'))
                                <p class="text-danger">{{$errors->first('image')}}</p>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label>Latitude</label>
                                <input type="text" name="latitude" value="{{old('latitude', $academy->latitude)}}" class="form-control">
                                @if($errors->has('latitude'))
                                    <p class="text-danger">{{$errors->first('latitude')}}</p>
                                @endif
                            </div>
                            <div class="col-sm-6 form-group">
                                <label>Longitude</label>
                                <input type="text" name="longitude" value="{{old('longitude', $academy->longitude)}}" class="form-control">
                                @if($errors->has('longitude'))
                                    <p class="text-danger">{{$errors->first('longitude')}}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" class="form-control">{{old('description', $academy->description)}}</textarea>
                            @if($errors->has('description'))
                                <p class="text-danger">{{$errors->first('description')}}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Tags</label>

                            <div class="select-box clearfix form-control">
                                <span v-for="tag in selected" track-by="$index" class="select-box-item label label-info">
                                    <input type="hidden" name="tags[@{{$index}}]" :value="tag">
                                    @{{tag}}
                                    <span @click="remove(tag)">&times;</span>
                                </span>
                                <input type="text" class="select-input" v-model="tag_input" @keyup.delete="clear" @keyup.space="add">
                            </div>
                            @if($errors->has('tags'))
                                <p class="text-danger">{{$errors->first('tags')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <h2>Time Slots</h2>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Sunday</label>
                            <input type="text" name="time_slots[sunday]" value="{{old('time_slots.sunday', $academy->timeSlots->where('day_of_week', 'sunday')->first()->time_slot)}}" class="form-control">
                            @if($errors->has('time_slots.sunday'))
                                <p class="text-danger">{{$errors->first('time_slots.sunday')}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Monday</label>
                            <input type="text" name="time_slots[monday]" value="{{old('time_slots.monday', $academy->timeSlots->where('day_of_week', 'monday')->first()->time_slot)}}" class="form-control">
                            @if($errors->has('time_slots.monday'))
                                <p class="text-danger">{{$errors->first('time_slots.monday')}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Tuesday</label>
                            <input type="text" name="time_slots[tuesday]" value="{{old('time_slots.tuesday', $academy->timeSlots->where('day_of_week', 'tuesday')->first()->time_slot)}}" class="form-control">
                            @if($errors->has('time_slots.tuesday'))
                                <p class="text-danger">{{$errors->first('time_slots.tuesday')}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Wednesday</label>
                            <input type="text" name="time_slots[wednesday]" value="{{old('time_slots.wednesday', $academy->timeSlots->where('day_of_week', 'wednesday')->first()->time_slot)}}" class="form-control">
                            @if($errors->has('time_slots.wednesday'))
                                <p class="text-danger">{{$errors->first('time_slots.wednesday')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Thursday</label>
                            <input type="text" name="time_slots[thursday]" value="{{old('time_slots.thursday', $academy->timeSlots->where('day_of_week', 'thursday')->first()->time_slot)}}" class="form-control">
                            @if($errors->has('time_slots.thursday'))
                                <p class="text-danger">{{$errors->first('time_slots.thursday')}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Friday</label>
                            <input type="text" name="time_slots[friday]" value="{{old('time_slots.friday', $academy->timeSlots->where('day_of_week', 'friday')->first()->time_slot)}}" class="form-control">
                            @if($errors->has('time_slots.friday'))
                                <p class="text-danger">{{$errors->first('time_slots.friday')}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Saturday</label>
                            <input type="text" name="time_slots[saturday]" value="{{old('time_slots.saturday', $academy->timeSlots->where('day_of_week', 'saturday')->first()->time_slot)}}" class="form-control">
                            @if($errors->has('time_slots.saturday'))
                                <p class="text-danger">{{$errors->first('time_slots.saturday')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" value="Update Academy" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        window.import = {
            tags: {!! json_encode($academy->tags->lists('tag_name')) !!}
        };
    </script>
    <script type="text/javascript" src="{{asset('js/create.js')}}"></script>
@stop