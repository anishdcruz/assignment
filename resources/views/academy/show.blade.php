@extends('layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <p class="panel-title">
                View Academy
                <a class="pull-right" href="{{route('academy.edit', $academy)}}">Edit</a>
            </p>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-3">
                    <div class="thumbnail">
                        <img src="{{asset('uploads/'.$academy->image)}}" alt="...">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="from-group">
                        <label>Username</label>
                        <p>{{$academy->username}}</p>
                    </div>
                    <div class="from-group">
                        <label>E-mail Address</label>
                        <p>{{$academy->email}}</p>
                    </div>
                    <div class="from-group">
                        <label>Telephone</label>
                        <p>{{$academy->phone}}</p>
                    </div>
                    <div class="from-group">
                        <label>Tags</label>
                        <p>
                            @foreach($academy->tags as $tag)
                                <span class="select-box-item label label-info">{{$tag->tag_name}}</span>
                            @endforeach
                        </p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="from-group">
                        <label>Academy Name</label>
                        <p>{{$academy->academy_name}}</p>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 from-group">
                            <label>Latitude</label>
                            <p>{{$academy->latitude}}</p>
                        </div>
                        <div class="col-sm-6 from-group">
                            <label>Longitude</label>
                            <p>{{$academy->longitude}}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <pre class="pre">{{$academy->description}}</pre>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Time Slots</label>
                        <p>
                            @foreach($academy->timeSlots as $slot)
                                @if($slot->time_slot)
                                    {{$slot->day_of_week}} - {{$slot->time_slot}}<br>
                                @else
                                    {{$slot->day_of_week}} - Nil<br>
                                @endif
                            @endforeach
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop