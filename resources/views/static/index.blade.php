@extends('layouts.master')

@section('content')
    <div class="row">

        <div class="col-sm-8 col-sm-offset-2">
            <div class="jumbotron">
                <h1>Jumbotron heading</h1>
                <p class="lead">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
                <p>
                    <a href="{{route('explore.index')}}" class="btn btn-success btn-lg text-center">Explore</a>
                    <a href="{{route('academy.index')}}" class="btn btn-default btn-lg text-center">Manage Academy</a>
                </p>
            </div>

        </div>
    </div>
@stop