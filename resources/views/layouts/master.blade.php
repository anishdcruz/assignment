<!DOCTYPE html>
<html>
<head>
    <title>{{$title}}</title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
    <meta name="viewport" content="initial-scale=1.0">
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">Assignment</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="{{route('explore.index')}}">Explore</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{route('academy.index')}}">Manage Academy</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        @if(session()->has('message'))
            <div class="alert alert-info">
                {{session()->get('message')}}
            </div>
        @endif
        @yield('content')
    </div>
    @yield('scripts')
</body>
</html>