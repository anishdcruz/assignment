@extends('layouts.master')

@section('content')
    <div class="map-container">
        <div id="map"></div>
    </div>
@stop

@section('scripts')
    <script>
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.9, lng: 151.2},
          zoom: 10
        });
        setMarkers(map);
      }
      var academies = {!! $academies !!};

      function setMarkers(map) {
        for (var i = 0; i < academies.length; i++) {
            var academy = academies[i];
            var marker = new google.maps.Marker({
              position: {lat: academy.latitude, lng: academy.longitude},
              map: map,
              title: academy.academy_name
            });
            addInfoWindow(marker, academy, map);
          }
      }

      function addInfoWindow(marker, academy, map) {
        var contentString = '<div id="content">'+
              '<div id="bodyContent">'+
              '<p><img class="marker-img" src="/uploads/' + academy.image + '"></p>'+
              '<p>' + academy.academy_name + '<br> <a href="/explore/' + academy.id + '">View Details</a></p>'+
              '</div>'+
              '</div>';
          var infoWindow = new google.maps.InfoWindow({
              content: contentString,
              maxWidth: 200
          });

          google.maps.event.addListener(marker, 'click', function () {
              infoWindow.open(map, marker);
          });
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('G_API')}}&callback=initMap" async defer></script>
@stop