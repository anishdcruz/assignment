@extends('layouts.master')

@section('content')
    <div class="row">
      <div class="col-sm-6">
        <div id="map"></div>
      </div>
      <div class="col-sm-6">
        <img src="{{asset('uploads/'.$academy->image)}}" class="marker-img">
        <h2>{{$academy->academy_name}}</h2>
        <h4>Description</h4>
        <pre class="pre">{{$academy->description}}</pre>

        <h4>Time Slots</h4>
        <p>
          @foreach($academy->timeSlots as $slot)
            <span>{{$slot->day_of_week}} - {{$slot->time_slot}}</span><br>
          @endforeach
        </p>
        <h4>Tags</h4>
        <p>
          @foreach($academy->tags as $tag)
            <span class="label label-default">{{$tag->tag_name}}</span>
          @endforeach
        </p>
      </div>
    </div>
@stop

@section('scripts')
    <script>
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.9, lng: 151.2},
          zoom: 10
        });
        setMarkers(map);
      }
      var academy = {!! $academy->toJson() !!};

      function setMarkers(map) {
        var marker = new google.maps.Marker({
          position: {lat: academy.latitude, lng: academy.longitude},
          map: map,
          title: academy.academy_name
        });
        addInfoWindow(marker, academy, map);
      }

      function addInfoWindow(marker, academy, map) {
        var contentString = '<div id="content">'+
              '<div id="bodyContent">'+
              '<p><img class="marker-img" src="/uploads/' + academy.image + '"></p>'+
              '<p>' + academy.academy_name + '<br> <a href="/explore/' + academy.id + '">View Details</a></p>'+
              '</div>'+
              '</div>';
          var infoWindow = new google.maps.InfoWindow({
              content: contentString,
              maxWidth: 200
          });

          google.maps.event.addListener(marker, 'click', function () {
              infoWindow.open(map, marker);
          });
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('G_API')}}&callback=initMap" async defer></script>
@stop