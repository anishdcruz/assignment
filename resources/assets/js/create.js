import Vue from 'vue'

var app = new Vue({
  el: 'body',
    data() {
       return {
          selected: window.import.tags,
          tag_input: ''
       }
    },
    computed: {
      value () {
        return JSON.stringify(this.selected)
      }
    },
    methods: {
      add() {
        var input = this.tag_input.trim()
        if(input.length <= 0) {
          return;
        }
        this.selected.push(this.tag_input)
        this.tag_input = ''
      },
      remove: function(tag) {
        this.selected.$remove(tag);
      },
      clear: function() {
        var input = this.tag_input.trim()
        if(input.length === 0) {
          this.selected.pop();
        }
      }
    }
})