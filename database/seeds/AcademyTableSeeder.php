<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Academy;
use App\AcademyTag;
use App\AcademyTimeSlot;

class AcademyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        Academy::truncate();
        AcademyTag::truncate();
        AcademyTimeSlot::truncate();

        foreach(range(1, 5) as $index) {
            $academy = Academy::create([
                'username' => $faker->userName,
                'email' => $faker->email,
                'phone' => $faker->phoneNumber,
                'academy_name' => 'Academy '.$index,
                'image' => 'demo'.$index.'.png',
                'description' => $faker->text,
                'latitude' => $faker->latitude,
                'longitude' => $faker->longitude
            ]);

            $tags = [];

            foreach(range(1, rand(1, 5)) as $i) {
                $tags[] = new AcademyTag([
                    'tag_name' => 'tag'.$i
                ]);
            }

            $slots = [];

            foreach(['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'] as $i) {
                $slots[] = new AcademyTimeSlot([
                    'day_of_week' => $i,
                    'time_slot' => '10AM - 3PM'
                ]);
            }


            $academy->tags()->saveMany($tags);
            $academy->timeSlots()->saveMany($slots);
        }
    }
}
